/* For preloader */
$(window).on('load', function(){
	//$('.preloader').delay(500).fadeOut('slow');
});

/* scripts ready */
$(document).ready(function(){


	/* меню */
	$("#menu").mmenu({
		"extensions": [
			"fx-panels-zoom",
			"theme-dark",
			"pagedim-black"
		],
		"offCanvas": {
			"position": "right"
		},
		"navbars": [
			{
					"position": "top",
					"content": [
						"searchfield"
					]
			},
			{
					"position": "top"
			},
			{
					"position": "bottom",
					"content": [
						"<a class='fa fa-vk' href='#/'></a>",
						"<a class='fa fa-twitter' href='#/'></a>",
						"<a class='fa fa-facebook' href='#/'></a>"
					]
			}
		]
	});

	$(".catalog__item img").equalHeights();
	$(".advantages__item").equalHeights();

	/* гамбургег */
	var api = $('#menu').data('mmenu');
	api.bind('open:finish', function(){
		$('.hamburger').addClass('is-active');
	});
	api.bind('close:finish', function(){
		$('.hamburger').removeClass('is-active');
	});

	$(".filter .filter__item > div").on('click', function(){
		var findFilteritem = $(this).siblings();
		var	findFilter = $(this).closest('.filter');

		if (findFilteritem.is(':visible')) {
			findFilteritem.slideUp();
    }
    else {
    	findFilter.find('.filter__subitem').slideUp();
    	findFilteritem.slideDown();
    }
	});


	/* filter js */
	var selectedClass = "";
	$(".filter__item").click(function(){ 
		selectedClass = $(this).attr("data-rel"); 
			console.log(selectedClass);
		
		$("#category-content").fadeTo(100, 0.1);
		$("#category-content .category-item").not("."+selectedClass).fadeOut();
		$(".filter .filter__item").removeClass('active');
		$(this).addClass('active');
		console.log(this);
		setTimeout(function() {

			$("."+selectedClass).fadeIn("slow");
			$("#category-content").fadeTo(300, 1);
	}, 300); 
		
	});




});
