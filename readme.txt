Проект 2 сокр. курса по предмету "Информационные технологии"

Название проекта - интернет-каталог моторных масел "MotorOil"

Участники:
Гобриянчик Владислав Сергеевич - Design / CSS / SASS
Кудёлко Андрей Александрович - Design / CSS / SASS / Angular2 / JS
Лихтар Кристина Владимировна - Business

Преподаватель - Перез Чернов А.Х.